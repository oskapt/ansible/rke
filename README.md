Ansible playbook for [rke](https://github.com/rancher/rke).

This actually doesn't do anything related to the `rke` binary. Instead,
it preps systems for an RKE install. Prerequisites like `docker` are handled
by the `docker` playbook. This won't fail if Docker isn't installed, but it
also won't install it.

## Notes

If your RKE installation is one that still carries the bug preventing you
from using password-backed SSH keys, you'll need to add an SSH key that
has permissions to ssh in as `root` on the RKE systems. Do this by 
overriding `rke_public_keys` with a list of keys permitted to log in as
`root`. If, for some reason, you don't use `root` for your RKE install, you
can also override `rke_ssh_users` with a list of users for whom the key
should be installed.